# -*- coding: utf-8 -*-

from django.views.generic import View
from django.shortcuts import render, redirect

import flow.step as step
import flow.result as result

#    ... ---> [FlowStep] => [FlowResult] -> [FlowStep] ...
#                               |
#                               v
#                            (exit)

#==========================================================================================================
class FakeBaseView(object):
    def renderTemplate(self, templateName):
        return True

#==========================================================================================================
class BaseView(View):
    flowGET  = [ ]
    flowPOST = [ ]

    #-----------------------------------------------------------
    def getViewParam(self, paramName, valOnError = None):
        try:
            GET = self.getContext('GET')
            return GET.get(paramName)
        
        except:
            return valOnError

    #-----------------------------------------------------------
    def renderTemplate(self, ctx, templateName):
        return render(ctx.get('request'), templateName, { 'params' : ctx.get('params') })

    #------------------------------------------------------------------------------------------------------        
    def _buildContext(self, request, *args, **kwargs):
        dCtx = { } 

        dCtx['request']      = request
        dCtx['args']         = args
        dCtx['kwargs']       = kwargs

        dCtx['user']         = None
        dCtx['isUserLogged'] = False
        dCtx['isuserAdmin']  = False

        dCtx['GET']          = None
        dCtx['POST']         = None
        dCtx['FILES']        = None

        try:
            user = request.user

            dCtx['user']         = user
            dCtx['isUserLogged'] = user and user.is_authenticated()
            dCtx['isUserAdmin']  = user and user.is_staff
        except:
            pass

        try:
            dCtx['GET'] = request.GET
        except:
            pass

        try:
            dCtx['POST'] = request.POST
        except:
            pass

        try:
            dCtx['FILES'] = request.FILES
        except:
            pass

        dCtx['DEBUG'] = step.FlowProcessor.DEBUG

        dCtx['params'] = { } # 'params' for html template rendering

        return step.Context(dCtx)

    #------------------------------------------------------------------------------------------------------        
    def get(self, request, *args, **kwargs):
        ctx = self._buildContext(request, args, kwargs)
        res = step.FlowProcessor.process(self, self.flowGET, ctx, result.ServerError('Error processing GET flow'))
        return result.ResultExecutor.execute(self, ctx, res)


    def post(self, request, *args, **kwargs):
        ctx = self._buildContext(request, args, kwargs)
        res = step.FlowProcessor.process(self, self.flowPOST, ctx, result.ServerError('Error processing POST flow'))
        return result.ResultExecutor.execute(self, ctx, res)
