# -*- coding: utf-8 -*-

from django.views.generic import View
from django.shortcuts import render, redirect

import result

import theflow.utils as utils

#==========================================================================================================
#
#   BASE
#
#==========================================================================================================
class FlowStep:
    def handle(self, bview, ctx):
        return result.ServerError()

class SimpleFlowStep(FlowStep):
    
    # to be overriden
    def _processHandle(self, bview, ctx):
        pass

    def handle(self, bview, ctx):
        self._processHandle(bview, ctx)
        return result.Continue()


#==========================================================================================================
#
#   CONTEXT
#
#==========================================================================================================
class Context:
    def __init__(self, ctx):
        self.ctx = ctx

    def get(self, name):
        try:
            return self.ctx[name]
        except:
            return None

    def set(self, name, value):
        try:
            self.ctx[name] = value
            return True
        except:
            return False

    def has(self, name):
        return name in self.ctx

#==========================================================================================================
#
#   FLOW PROCESSOR
#
#==========================================================================================================
class FlowProcessor:
    DEBUG = True

    @classmethod
    def process(cls, bview, flow, ctx, defaultResult):
        try:
            if FlowProcessor.DEBUG:
                print '[flow] - begin'
            
            for item in flow:
                if FlowProcessor.DEBUG:
                    print '[flow] >', item.__class__
            
                res = item.handle(bview, ctx)

                if res.mustExit():
                    if FlowProcessor.DEBUG:
                        print '[flow] - exit >', res.__class__

                    return res

            if FlowProcessor.DEBUG:
                print '[flow] - end'

            return defaultResult

        except Exception as ex:
            if FlowProcessor.DEBUG:
                print '[flow] !!!', ex

            return result.ServerError(unicode(ex))


#==========================================================================================================
#
#   TEMPLATES
#
#==========================================================================================================
class RenderTemplate(FlowStep):
    def __init__(self, templateName):
        self.templateName = templateName

    def handle(self, bview, ctx):
        return result.RenderTemplate(self.templateName)


#==========================================================================================================
#
#   USER
#
#==========================================================================================================
class Logout(FlowStep):
    def __init__(self, redirectName = 'home'):
        self.redirectName = redirectName

    def handle(self, bview, ctx):
        return result.Logout(self.redirectName)

#------------------------------------------------------------------------------------
class IsUserLoggedOrRedirect(FlowStep):
    def __init__(self, redirectName = 'login'):
        self.redirectName = redirectName

    def handle(self, bview, ctx):
        if ctx.get('isUserLogged'):
            return result.Continue()

        else:
            return result.Redirect(self.redirectName)

#------------------------------------------------------------------------------------
class IsUserLoggedOrError(FlowStep):
    def __init__(self, errMsg = 'User must be logged'):
        self.errMsg = errMsg

    def handle(self, bview, ctx):
        if ctx.get('isUserLogged'):
            return result.Continue()

        else:
            return result.UserError(self.errMsg)

#------------------------------------------------------------------------------------
class RoleInListOrError(FlowStep):
    def __init__(self, lstRoles):
        self.lstRoles = lstRoles

    def handle(self, bview):
        user = ctx.get('user')

        if user.userprofile.getSysRoleID() in self.lstRoles:
            return result.Continue()        
        
        else:
            return result.UserError('You are not allowed to enter here')


#==========================================================================================================
#
#   REDIRECTION
#
#==========================================================================================================
class Redirect(FlowStep):
    def __init__(self, redirectName):
        self.redirectName = redirectName

    def handle(self, bview, ctx):
        return result.Redirect(self.redirectName)

#------------------------------------------------------------------------------------
class RedirectIfUserLogged(FlowStep):
    def __init__(self, redirectName):
        self.redirectName = redirectName

    def handle(self, bview, ctx):
        if ctx.get('isUserLogged'):
            return result.Redirect(self.redirectName)

        else:
            return result.Continue()

#------------------------------------------------------------------------------------
class RedirectIfRole(FlowStep):
    def __init__(self, roleID, redirectName):
        self.roleID = roleID
        self.redirectName = redirectName    

    def handle(self, bview):
        user = ctx.get('user')

        if user.userprofile.getSysRoleID() == self.roleID:
            return result.Redirect(self.redirectName)
        
        else:
            return result.Continue()        

#==========================================================================================================
#
#   VIEW PARAMS & OBJECTS
#
#==========================================================================================================
class GetViewParams(FlowStep):
    def __init__(self, lstParam):   # lstParam = [ ... 'nameParam' ... ]  or lstParam = [ ... (nameGlobal, nameParam) ... ] or mixed
        self.lstParam = lstParam

    def handle(self, bview, ctx):
        for name in self.lstParam:
            try:
                nameGlobal, nameParam = name
            except:
                nameParam  = name
                nameGlobal = name

            viewParam = bview.getViewParam(nameParam)

            if not viewParam:
                return result.ServerError('No param "%s" found' % (nameParam, ))
            
            else:
                ctx.set(nameGlobal, viewParam)

        return result.Continue()


#------------------------------------------------------------------------------------
class GetViewParamsOrError(FlowStep):
    def __init__(self, lstParam):
        self.lstParam = lstParam

    def handle(self, bview, ctx):
        for name in self.lstParam:
            try:
                nameGlobal, nameParam = name
            except:
                nameParam  = name
                nameGlobal = name

            viewParam = bview.getViewParam(nameParam)

            if not viewParam:
                return result.ServerError('No param "%s" found' % (nameParam, ))
            
            else:
                ctx.set(nameGlobal, viewParam)

        return result.Continue()


#------------------------------------------------------------------------------------
class FindObjectById(FlowStep):
    def __init__(self, strParam, clsObject, objName = None):
        self.strParam   = strParam
        self.clsObject  = clsObject
        self.objName    = objName

        if self.objName == None:
            self.objName = self.strParam

    def handle(self, bview, ctx):
        viewParam = bview.getViewParam(self.strParam)

        if not viewParam:
            return result.ServerError('No param "%s" found' % (self.strParam, ))

        try:            
            idObj = int(viewParam)
            
            obj = self.clsObject.getByID(idObj) # the idea is to get an object by its ID
            
            ctx.set(self.objName, obj)

            return result.Continue()
        
        except:
            return result.UserError('No object found of type %s with id = %s' % (unicode(self.clsObject), self.strParam))


#==========================================================================================================
#
#   FORM
#
#==========================================================================================================
class InitForm(SimpleFlowStep):
    def __init__(self, clsForm):
        self.clsForm = clsForm

    def _processHandle(self, bview, ctx):
        form = self.clsForm(label_suffix = '')
        ctx.set('form', form)

        return result.Continue()

#------------------------------------------------------------------------------------
class FormToHtml(FlowStep):
    def __init__(self, initObjName = None):
        self.initObjName = initObjName

    def handle(self, bview, ctx):
        params   = ctx['params']
        form     = ctx['form']
        initObj  = ctx[self.initObjName]

        # -------
        if not form:
            return result.UserError('Parameter "form" has not be initialized')

        # -------
        dp = None

        if initObj:
            dp = {}
            dp[self.initObjName] = initObj

        form.fillHTMLParams(params, dp)

        return result.Continue()

#------------------------------------------------------------------------------------
class FormToHtmlList(FlowStep):
    def handle(self, bview, ctx):
        params = ctx.get('params')
        form   = ctx.get('form')

        pform = []

        for name, field in form.fields.items():
            fld = utils.formFieldToHtmlParam(form, name, field)
            
            pform.append(fld)

        params['form'] = pform

        return result.Continue()

#------------------------------------------------------------------------------------
class ProcessForm(FlowStep):
    def __init__(self, clsForm):
        self.clsForm = clsForm
    
    def handle(self, bview, ctx):
        POST  = ctx.get('POST')

        if not POST:
            return result.UserError('No POST :(')

        else:
            form = self.clsForm(POST)
            ctx.set('form', form)

            # init field "values" 
            for fieldname in self.clsForm.field_order:
                field = form.fields[fieldname]
                field.value = None

            return result.Continue()


#------------------------------------------------------------------------------------
class ProcessFormWithFiles(FlowStep):
    def __init__(self, clsForm):
        self.clsForm = clsForm
    
    def handle(self, bview, ctx):
        POST  = ctx.get('POST')
        FILES = ctx.get('FILES')

        if not FILES or not POST:
            return result.UserError('No POST and/or no FILES :(')

        else:
            form = self.clsForm(POST, FILES)
            ctx.set('form', form)

            # init field "values" 
            for fieldname in self.clsForm.field_order:
                field = form.fields[fieldname]
                field.value = None

            return result.Continue()

#------------------------------------------------------------------------------------
class ValidateFormOr(FlowStep):
    def __init__(self, flowOnInvalid, defaultResult):
        self.flowOnInvalid = flowOnInvalid
        self.defaultResult = defaultResult
    
    def handle(self, bview, ctx):
        form = ctx.get('form')
        POST = ctx.get('POST')

        # -------
        if not POST:
            return result.UserError("There's no POST data")

        if not form:
            return result.UserError('Parameter "form" has not be initialized')

        # -------
        if form.isValid(POST):
            return result.Continue()

        else:
            return FlowProcessor.process(bview, self.flowOnInvalid, ctx, self.defaultResult)


#------------------------------------------------------------------------------------
class UpdateFormOnError(FlowStep):
    def handle(self, bview, ctx):
        params  = ctx.get('params')
        form    = ctx.get('form')
        POST    = ctx.get('POST')
        FILES   = ctx.get('FILES')

        # -------
        if not POST:
            return result.UserError("There's no POST data")

        if not form:
            return result.UserError('Parameter "form" has not be initialized')
        
        # -------
        try:
            if FILES:
                form.update(params, POST, FILES)

            else:
                form.update(params, POST)

            return result.Continue()

        except Exception, ex:
            return result.UserError('The form could not be updated (err_msg: %s)' % (unicode(ex), ))


#------------------------------------------------------------------------------------
class UpdateFormWithSubmittedData(FlowStep):
    def __init__(self, clsForm):
        self.clsForm = clsForm

    def handle(self, bview, ctx):
        form = ctx.get('form')

        lstFields = self.clsForm.field_order
        for fieldname in lstFields:
            field = form.fields[fieldname]

            submitted = utils.getFormFieldSubmittedValue(form, fieldname, None)

            if submitted:
                field.value = submitted

        return result.Continue()
