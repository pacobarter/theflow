# -*- coding: utf-8 -*-

from django.shortcuts import redirect
from django.contrib.auth import logout

#==========================================================================================================
class FlowResult:
    def mustExit(self):
        return True

    def getResult(self, bview):
        return None

#==========================================================================================================
class ResultExecutor:
    @classmethod
    def execute(cls, bview, ctx, flowResult):
        res = flowResult.getResult(bview, ctx)

        if not res:
            raise Exception('A FlowResult cannot exec to None')

        return res


#==========================================================================================================
class Continue(FlowResult):
    def mustExit(self):
        return False

#==========================================================================================================
class Redirect(FlowResult):
    def __init__(self, redirectName):
        self.redirectName = redirectName

    def getResult(self, bview, ctx):
        return redirect(self.redirectName)

#==========================================================================================================
class Logout(FlowResult):
    def __init__(self, redirectName):
        self.redirectName = redirectName

    def getResult(self, bview, ctx):
        request = ctx.get('request')
        logout(request)
        return redirect(self.redirectName)

#==========================================================================================================
class RenderTemplate(FlowResult):
    def __init__(self, templateName):
        self.templateName = templateName

    def getResult(self, bview, ctx):
        return bview.renderTemplate(ctx, self.templateName)

#==========================================================================================================
class Error(FlowResult):
    def __init__(self, errMsg):
        self.errMsg = errMsg

    def getResult(self, bview, ctx):
        params = ctx.get('params')

        p = {}
        p['message']    = self.errMsg

        params['error'] = p

        return bview.renderTemplate(ctx, 'pages/error.html')


#-----------------------------------------------------------------------------------------
class ServerError(Error):
    def __init__(self, errMsg = 'Server error :('):
        self.errMsg = errMsg


#-----------------------------------------------------------------------------------------
class UserError(Error):
    def __init__(self, errMsg = 'You are not allowed to do that'):
        self.errMsg = errMsg

