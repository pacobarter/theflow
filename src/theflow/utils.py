# -*- coding: utf-8 -*-

#------------------------------------------------------------------------------------
#    Forms utils
#
def getFormFieldSubmittedValue(form, fieldName, defaultVal = None):
    try:
        return form.data[fieldName]

    except:
        return defaultVal    


def formFieldToHtmlParam(form, name, field):
    fld = {}
    fld['name']         = name
    fld['required']     = field.required
    fld['label']        = field.label
    fld['min_length']   = field.min_length
    fld['max_length']   = field.max_length
    fld['initial']      = field.initial
    fld['help_text']    = field.help_text
    fld['type']         = form.field_types[name]

    # "value" isn't part of Django
    try:
        fld['value'] = field.value
    except:
        fld['value'] = None

    try:
        fld['error'] = form.errors[name][0]
    except:
        fld['error'] = None
    
    fld['has_error'] = fld['error'] != None

    return fld

