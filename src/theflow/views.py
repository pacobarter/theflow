# -*- coding: utf-8 -*-

from .base.views import BaseView
import base.flow.step as step

#------------------------------------------------------------------------------------
#    Home view
#
class HomeView(BaseView):
    flowGET = [
        step.RedirectIfUserLogged('userprofile.view'),
        step.RenderTemplate('pages/home.html')
    ]

#------------------------------------------------------------------------------------
#    Main View
#
class MainView(BaseView):
    flowGET = [
        step.IsUserLoggedOrRedirect(),
        step.RenderTemplate('pages/main.html')
    ]

#------------------------------------------------------------------------------------
#    Logout
#
class LogoutView(BaseView):
    flowGET = [
        step.Logout()
    ]