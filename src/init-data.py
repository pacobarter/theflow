# -*- coding: utf-8 -*-

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "theflow.settings")

import django
django.setup()

from theflow import const
from userprofile.models import UserProfile

#--------------------------------------------------------------
#
params = {}
params['idSysRole']     = const.ID_SYSROLE_ACCOUNT_MANAGER
params['username']      = 'frank'
params['password']      = 'frankManager'
params['email']         = 'fran@company.com'
params['firstname']     = 'Frank'
params['lastname']      = 'Carter'

UserProfile.createOne(params)

#--------------------------------------------------------------
#
params = {}
params['idSysRole']     = const.ID_SYSROLE_CUSTOMER_USER
params['username']      = 'aretha'
params['password']      = 'arethaUser'
params['email']         = 'aretha@other-company.com'
params['firstname']     = 'Aretha'
params['lastname']      = 'Franklin'

UserProfile.createOne(params)

