# -*- coding: utf-8 -*-

from django.shortcuts import render

import theflow.base.flow.step    as base_step
import theflow.base.flow.result  as base_result
import theflow.base.views        as base_views

import userprofile.flow.step     as userprofile_step

from .forms import UserprofileForm

#==========================================================================================================

BASE_GET = [
    base_step.IsUserLoggedOrRedirect(),
    userprofile_step.FillUserInfo(),
]

BASE_POST = [
    base_step.IsUserLoggedOrRedirect(),
    userprofile_step.FillUserInfo(),
]

#------------------------------------------------------------------------------------
#    "Logout" view
#
class LogoutView(base_views.BaseView):
    flowGET = [
        base_step.IsUserLoggedOrRedirect(),
        base_step.Logout()
    ]

#------------------------------------------------------------------------------------
#    User details view
#
class UserProfileView(base_views.BaseView):
    flowGET  = BASE_GET + [ 
        base_step.RenderTemplate('pages/userprofile/details.html')
    ]


#------------------------------------------------------------------------------------
#    Edit user details view
#
class UserProfileEdit(base_views.BaseView):
    template = 'pages/userprofile/edit.html'

    flowGET  = BASE_GET + [ 
        base_step.InitForm(UserprofileForm),
        userprofile_step.PrepareFormForEdit(),
        base_step.FormToHtmlList(),
        base_step.RenderTemplate(template)
    ]

    flowPOST = BASE_POST + [
        base_step.ProcessForm(UserprofileForm),
        userprofile_step.PrepareFormForEdit(),
        base_step.ValidateFormOr([
            base_step.UpdateFormWithSubmittedData(UserprofileForm),
            base_step.FormToHtmlList(), 
            base_step.RenderTemplate(template)
        ], base_result.ServerError('Error processing the query')),
        userprofile_step.UpdateUser(),
        base_step.Redirect('userprofile:view')
    ]


#------------------------------------------------------------------------------------
#    New User view
#
class NewUserView(base_views.BaseView):
    template = 'pages/userprofile/new.html'

    flowGET  = BASE_GET + [ 
        base_step.InitForm(UserprofileForm),
        base_step.FormToHtmlList(),
        base_step.RenderTemplate(template)
    ]

    flowPOST = BASE_POST + [
        base_step.ProcessForm(UserprofileForm),
        base_step.ValidateFormOr([
            base_step.UpdateFormWithSubmittedData(UserprofileForm),
            base_step.FormToHtmlList(), 
            base_step.RenderTemplate(template)
        ], base_result.ServerError('Error processing the query')),
        userprofile_step.NewUser(),
        base_step.Redirect('main')
    ]

