# -*- coding: utf-8 -*-

import theflow.base.flow.step   as step
import theflow.base.flow.result as result

from userprofile.models import UserProfile

#------------------------------------------------------------------------------------
class FillUserInfo(step.FlowStep):
    def handle(self, bview, ctx):
        params = ctx.get('params')

        user = ctx.get('user')

        UserProfile.fillHTMLParams(params, user.userprofile)

        return result.Continue()        

#------------------------------------------------------------------------------------
class PrepareFormForEdit(step.FlowStep):
    def handle(self, bview, ctx):
        user   = ctx.get('user')
        form   = ctx.get('form')

        form.setRequired('email',     False)
        form.setRequired('firstname', False)
        form.setRequired('lastname',  False)

        form.setInitial('email',     user.email)
        form.setInitial('firstname', user.first_name)
        form.setInitial('lastname',  user.last_name)

        return result.Continue()

#------------------------------------------------------------------------------------
class NewUser(step.FlowStep):
    def handle(self, bview, ctx):
        user = ctx.get('user')
        form = ctx.get('form')

        if ctx.get('DEBUG'):
            print '>>> NEW USER'
            print '>>>', form

        return result.Continue()        

#------------------------------------------------------------------------------------
class UpdateUser(step.FlowStep):
    def handle(self, bview, ctx):
        user = ctx.get('user')
        form = ctx.get('form')

        profile = user.userprofile

        firstName = form.getFirstname()
        if firstName:
            profile.setFirstName(firstName)

        lastName = form.getLastname()
        if lastName:
            profile.setLastName(lastName)

        email = form.getEmail()
        if email:
            profile.setEmail(email)

        return result.Continue()        
