# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from datetime import date

from django.db import models
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone

from theflow import const

#--------------------------------------------------------------------------------------------------------------
#     UserProfile
#
class UserProfile(models.Model):
    user = models.OneToOneField(User)

    idSysRole = models.IntegerField(choices = const.LST_SYSROLE, default = const.ID_SYSROLE_CUSTOMER_USER, verbose_name = 'System role')

    #-------------------------------------------------------------------------------------------
    #   BUILDER

    @classmethod
    def createOne(cls, params):
        '''
        params
            - idSysRole       (int)
            - username        (str)
            - password        (str)
            - email           (str)
            - firstname       (str)
            - lastname        (str)
        '''
        try:
            idSysRole   = params['idSysRole']
            username    = params['username']
            password    = params['password']
            email       = params['email']
            firstname   = params['firstname']
            lastname    = params['lastname']
            
            user = User()
            
            user.username       = username
            user.first_name     = firstname
            user.last_name      = lastname
            user.email          = email
            
            user.date_joined    = date.today()
            user.is_active      = True
            user.is_staff       = False
            user.is_superuser   = False
            
            user.set_password(password)
            
            user.save()

            userprofile = user.userprofile
            userprofile.idSysRole = idSysRole
            userprofile.save()
            
            return (True, userprofile)
        
        except Exception, ex:
            return (False, ex)

    #------------------------------------------------------
    #
    @classmethod
    def getName(cls, user):
        try:
            return user.userprofile.getUserName()
        except:
            return '?'
    
    @classmethod
    def fillHTMLParams(cls, params, userProfile, keyName = 'userprofile'):
        params[keyName] = {}
        
        if userProfile:
            params[keyName]['id']                   = userProfile.getUserID()
            params[keyName]['enabled']              = userProfile.isEnabled()
            params[keyName]['userName']             = userProfile.getUserName()
            params[keyName]['firstName']            = userProfile.getFirstName()
            params[keyName]['lastName']             = userProfile.getLastName()
            params[keyName]['email']                = userProfile.getEmail()
            params[keyName]['fullName']             = userProfile.getFullName()
            
            params[keyName]['sysRole']              = userProfile.getSysRoleName()
            
            params[keyName]['isSysAdmin']           = userProfile.isAdmin()
            params[keyName]['isSysAccManager']      = userProfile.isAccountManager()
            params[keyName]['isSysCustomerManager'] = userProfile.isCustomerManager()
            params[keyName]['isSysCustomerUser']    = userProfile.isCustomerUser()

    @classmethod
    def getByID(cls, idUser):
        try:
            return UserProfile.objects.get(user__id = idUser)
        
        except:
            return None

    @classmethod
    def findAdmins(cls, userToExclude = None):
        try:
            query = UserProfile.objects.filter(idSysRole = const.ID_SYSROLE_ADMIN)

            if userToExclude:
                query.exclude(user = userToExclude)
    
            return query

        except:
            return None

    @classmethod
    def findAllAccMngr(cls):
        try:
            return UserProfile.objects.filter(idSysRole = const.ID_SYSROLE_ACCOUNT_MANAGER)
        
        except:
            return None

    @classmethod
    def findAccMngr(cls, idAccMngr):
        if idAccMngr < 0:
            return None

        try:
            obj = UserProfile.objects.get(user__id = idAccMngr)
            
            if obj and obj.idSysRole == const.ID_SYSROLE_ACCOUNT_MANAGER:
                return obj
            
            else:
                return None

        except:
            return None 


    @classmethod
    def getAllLoggedUsers(cls, userToExclude = None):
        try:
            lstUserID = []
            
            # get active sessions and find userID
            sessions = Session.objects.filter(expire_date__gte = timezone.now())

            for session in sessions:
                data = session.get_decoded()
                lstUserID.append(data.get('_auth_user_id', None))

            # query all logged in users based on id list
            query = User.objects.filter(id__in = lstUserID)

            if userToExclude:
                query = query.exclude(id = userToExclude.id)

            return query

        except Exception, ex:
            return None


    @classmethod
    def getAllLoggedUsersCount(cls, userToExclude = None):
        query = UserProfile.getAllLoggedUsers(userToExclude)

        if query:
            return query.count()
        
        else:
            return 0


    @classmethod
    def isRoleAdmin(cls, user):
        return user.userprofile.isAdmin()

    @classmethod
    def isRoleAccountManager(cls, user):
        return user.userprofile.isAccountManager()

    @classmethod
    def isRoleManager(cls, user):
        return user.userprofile.isCustomerManager()

    @classmethod
    def isRoleUser(cls, user):
        return user.userprofile.isCustomerUser()
    
    @classmethod
    def isRoleUnk(cls, user):
        return user.userprofile.isRoleUnkown()

    #------------------------------------------------------
    #
    def __unicode__(self):
        user = self.user
        return '[profile:%s] %s : %s' % (self.getSysRoleName(), user.get_username(), user.get_full_name())

    #------------------------------------------------------
    #
    def getUser(self):
        return self.user

    #------------------------------------------------------
    #
    def isEnabled(self):
        return self.getUser().is_active == True
    
    def isLogged(self):
        query = UserProfile.getAllLoggedUsers()
#        query = query.filter(id=self.getUserID())
#        return query.count() > 0
        return self.getUser() in query

    #------------------------------------------------------
    #
    def getUserID(self):
        return self.user.pk

    def getUserName(self):
        return unicode(self.user.username)

    def getFirstName(self):
        return unicode(self.user.first_name)

    def getLastName(self):
        return unicode(self.user.last_name)

    def getFullName(self):
        return self.getFirstName() + ' ' + self.getLastName()

    def getEmail(self):
        return unicode(self.user.email)

    def getSysRoleID(self):
        return int(self.idSysRole)

    def getSysRoleName(self):
        try:
            return const.DICT_NAMES_SYSROLE[self.getSysRoleID()]
        
        except:
            return '?'

    #------------------------------------------------------
    #
    def isAdmin(self):
        return self.getSysRoleID() == const.ID_SYSROLE_ADMIN
    
    def isAccountManager(self):
        return self.getSysRoleID() == const.ID_SYSROLE_ACCOUNT_MANAGER

    def isCustomerManager(self):
        return self.getSysRoleID() == const.ID_SYSROLE_CUSTOMER_MANAGER

    def isCustomerUser(self):
        return self.getSysRoleID() == const.ID_SYSROLE_CUSTOMER_USER
    
    def isRoleUnkown(self):
        roleID = self.getSysRoleID()
        return roleID != const.ID_SYSROLE_ADMIN \
            and roleID != const.ID_SYSROLE_ACCOUNT_MANAGER \
            and roleID != const.ID_SYSROLE_CUSTOMER_MANAGER \
            and roleID != const.ID_SYSROLE_CUSTOMER_USER

    #------------------------------------------------------
    #
    def getCompaniesOwned(self):
        if self.isAdmin() or self.isAccountManager():
            return self.user.companiesOwned.all()
        
        else:
            return None

    #------------------------------------------------------
    #
    def setPassword(self, password):
        self.getUser().set_password(password)
        self.getUser().save()

    def resetPassword(self):
        self.setPassword(self.getUserName())

    def setFirstName(self, firstName):
        self.user.first_name = firstName
        self.getUser().save()

    def setLastName(self, lastName):
        self.user.last_name = lastName
        self.getUser().save()

    def setEmail(self, email):
        self.user.email = email
        self.getUser().save()
    
    def setEnabled(self):
        self.getUser().is_active = True
        self.getUser().save()
    
    def setDisabled(self):
        self.getUser().is_active = False
        self.getUser().save()

    def setAsCustomerManager(self):
        self.idSysRole = const.ID_SYSROLE_CUSTOMER_MANAGER
        self.save()

    def setAsCustomerUser(self):
        self.idSysRole = const.ID_SYSROLE_CUSTOMER_USER
        self.save()
    
    
#
User.userprofile = property(lambda u: UserProfile.objects.get_or_create(user = u)[0])
