# -*- coding: utf-8 -*-

from django import forms

class UserprofileForm(forms.Form):
    firstname   = forms.CharField(required = True,  max_length = 30, label = 'First Name', initial = u'Enter the first name', help_text = 'Max 30 chars')
    lastname    = forms.CharField(required = True,  max_length = 30, label = 'Last Name',  initial = u'Enter the last name',  help_text = 'Max 30 chars')
    email       = forms.EmailField(required = True, max_length = 50, label = 'Email'   ,   initial = u'Enter a valid email',  help_text = 'Max 50 chars')

    field_order = ['email', 'firstname', 'lastname']

    field_types = {
        'email'     : 'email', 
        'firstname' : 'text', 
        'lastname'  : 'text'
    }

    #-----------
    def isValid(self, POST):
        return self.is_valid()

    def setInitial(self, fieldname, initial):
        try:
            self.fields[fieldname].initial = initial
        except:
            pass

    def setRequired(self, fieldname, required):
        try:
            self.fields[fieldname].required = required
        except ex:
            pass

    #-----------
    def getFirstname(self):
        try:
            return self.cleaned_data['firstname']
        except:
            return None

    def getLastname(self):
        try:
            return self.cleaned_data['lastname']
        except:
            return None

    def getEmail(self):
        try:
            return self.cleaned_data['email']
        except:
            return None


