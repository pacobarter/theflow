# -*- coding: utf-8 -*-

from django.conf.urls import url

from django.contrib import admin
admin.autodiscover()

from .views import UserProfileView
from .views import UserProfileEdit
from .views import LogoutView
from .views import NewUserView

urlpatterns = [

    #     view user profile
    url(r'^$', UserProfileView.as_view(), name = 'view'),

    #    edit user profile
    url(r'^edit/?$', UserProfileEdit.as_view(), name = 'edit'),

    #    new user
    url(r'^new/?$', NewUserView.as_view(), name = 'new'),

    #     logout user
    url(r'^logout/?$', LogoutView.as_view(), name = 'logout'),

]